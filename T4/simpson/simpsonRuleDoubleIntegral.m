%% Helper for calculating simpson rule
%% @input f   : function to be approximated,
%% @input a   : lower bound for x,
%% @input b   : upper bound for x,
%% @input c   : lower bound for y,
%% @input d   : upper bound for y,
%% @output res: simpson rule result

function res = simpsonRuleDoubleIntegral(f, a, b, c, d)
    if nargin ~= 5
        error('Expecting 5 arguments');
    end

    res  = ((b-a)*(d-c)/36) * (16*f((b+a)/2, (d+c)/2) +
                                4*f((b+a)/2, d) + 4*f((b+a)/2, c) +
                                4*f(a, (d+c)/2) + 4*f(b, (d+c)/2) +
                                f(a, d) + f(b, d) + f(a, c) + f(b, c));
end