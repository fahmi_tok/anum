%% Function for approximating bounded integral with adaptive simpson quadrature
%% function and interval is hardcoded
%% @input tol   : error toleration,
%% @output res : final approximated result
%% @output iter : number of function calculated

function [res, iter] = adaptiveSimpsonQuadrature(tol)

    if nargin ~= 1
        error('Expecting 1 arguments');
    end

	f = inline('e^y * cos(x)');
    c = 0; d = 1; a = 0; b = pi/2;

    [res, iter] = adaptiveSimpsonQuadratureDoubleIntegralRecursion(f, a, b, c, d, tol, simpsonRuleDoubleIntegral(f, a, b, c, d), 1);

end