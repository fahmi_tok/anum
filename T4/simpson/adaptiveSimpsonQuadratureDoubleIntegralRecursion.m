%% Helper for calculating adaptive simpson quadrature with recursion
%% @input f   : function to be approximated,
%% @input a   : lower bound for x,
%% @input b   : upper bound for x,
%% @input c   : lower bound for y,
%% @input d   : upper bound for y,
%% @input tol   : error toleration,
%% @input whole : simpson rule result for whole interval,
%% @input depth   : depth of the recursion,
%% @output res : final approximated result
%% @output iter : number of function calculated

function [res, iter] = adaptiveSimpsonQuadratureDoubleIntegralRecursion(f, a, b, c, d, tol, whole, depth)
    if nargin ~= 8
        error('Expecting 8 arguments');
    end

    MAX_DEPTH = 100;
    p = (a+b)/2;
    q = (c+d)/2;

    lower_left = simpsonRuleDoubleIntegral(f, a, p, c, q);
    upper_left = simpsonRuleDoubleIntegral(f, a, p, q, d);
    lower_right = simpsonRuleDoubleIntegral(f, p, b, c, q);
    upper_right = simpsonRuleDoubleIntegral(f, p, b, q, d);

    left = upper_left+lower_left;
    right = upper_right+lower_right;

    if abs(left + right - whole) <= 15*tol || depth >= MAX_DEPTH
        res = left + right + (left + right - whole)/15;
        iter = 1;
        return;
    end
    [ul_res, ul_iter] = adaptiveSimpsonQuadratureDoubleIntegralRecursion(f, a, p, q, d, tol/4, upper_left, depth + 1);
    [ll_res, ll_iter] = adaptiveSimpsonQuadratureDoubleIntegralRecursion(f, a, p, c, q, tol/4, lower_left, depth + 1);
    [ur_res, ur_iter] = adaptiveSimpsonQuadratureDoubleIntegralRecursion(f, p, b, q, d, tol/4, upper_right, depth + 1);
    [lr_res, lr_iter] = adaptiveSimpsonQuadratureDoubleIntegralRecursion(f, p, b, c, q, tol/4, lower_right, depth + 1);
    res = ul_res + ur_res + ll_res + lr_res;
    iter = ul_iter + ur_iter + ll_iter + lr_iter + 1;
    return
end