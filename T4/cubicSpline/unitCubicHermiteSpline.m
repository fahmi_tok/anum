%% calculate a spline interpolation of a given x value
%% @input x     : the x value of endpoints (array or vector of 2 values)
%% @input y     : the y value of endpoints (array or vector of 2 values)
%% @input m     : the gradient of endpoints (array or vector of 2 values)
%% @input curX  : the x value to be approximated
%% @output curY : the y value result of spline interpolation approximation
function[curY] = unitHermiteSpline(x, y, m, h, curX)
  i = 1;
  h = abs(x(2) - x(1));
  t = (curX - x(i)) / h;
  h00 = 2 * t * t * t - 3 * t * t + 1;
  h10 = t * t * t - 2 * t * t + t;
  h01 = -2 * t * t * t + 3 * t * t;
  h11 = t * t * t - t * t;

  curY = h00 * y(i) + h10 * h * m(i) + h01 * y(i + 1) + h11 * h * m(i + 1);
end