%% Cubic spline generator
%% use Hermite basis function
%% assume natural endpoints
%% f(x) used is Runge Function, however interpolation process does not know the form of f(x)
%% interval bound by [-4,4]
%% @input h      : the length of each interval to generate spline
%% @output xRes  : points in each spline with interval 0.001 for graph plotting purpose
%% @output yRes  : spline interpolation of f(xRes)
%% @output err   : error computed by maximum relative error of y at point (x(R)-x(L))/2 in each spline
function[xRes, yRes, err] = rungeFunctionCubicHermiteSpline(h)
  % generate points to be interpolated
  x = [];
  y = [];
  for i=-4:h:4
    x = vertcat(x, i);
    y = vertcat(y, runge(i));
  endfor
  [n,~] = size(x);
  
  % m(k) is gradient of spline at point x(k)
  % solve linear equation m(k) + m(k + 1) + 4m(k + 2) = 3 * (y(k + 2) - y(k)) / h
  % natural endpoints : m(1) = m(n) = 0
  m = [0];
  a = [0];
  for i=1:n-2
    m = vertcat(m, 3 * (x(i+2) - x(i)) / h);
    a = vertcat(a, 4);
  endfor
  m = vertcat(m, 0);
  a = vertcat(a, 0);
  for i=2:n-1
    m(i) = m(i) - m(i - 1);
    a(i) = a(i) - a(i - 1);
    m(i) = m(i)/a(i);
    a(i) = 1/a(i);
  endfor
  
  for i=n-1:-1:2
    m(i) = m(i) - a(i) * m(i + 1);
  endfor
  
  % calculate the cubic hermite spline
  xRes = [];
  yRes = [];
  err = 0;
  for i=1:n-1
    %error calculation
    curX = ((x(i) + x(i + 1)) / 2);
    curY = unitHermiteSpline(x(i:i+1), y(i:i+1), m(i:i+1), curX);
    err = max(err, abs((runge(curX) - curY) / runge(curX)));
    
    %generate points for graph plotting
    for j=x(i):0.001:x(i+1)
      xRes = vertcat(xRes, j);
      yRes = vertcat(yRes, unitHermiteSpline(x(i:i+1), y(i:i+1), m(i:i+1), h, j));
    endfor
  endfor
  
end