%% Helper for calculating runge function
%% @input x  : value of x
%% @output y : value of y by runge function of x
function[y] = runge(x)
  y = 10 / (1 + 25 * x * x);
end