%% Function for calculating newton coefficients of newton interpolation in O(m^2)
%% using divided difference table
%% @input points : an m x 2 matrix of (t, y) points
%%
%% @output a : newton polynomial coefficients a, in the form [a_0, a_1, ..., a_m-1]
function [a] = calculateNewtonCoef(points)
    m = length(points);
    helper = points(:,2);
    a = zeros(m, 1);
    a(1) = helper(1);
    
    for i = 2:m
        for j = m:-1:i
            leftmost = j-i+1;
            helper(j) = (helper(j) - helper(j-1)) / (points(j, 1) - points(leftmost, 1));
        end
        a(i) = helper(i);
    end
end