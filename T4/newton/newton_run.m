function [] = newton_run()
    t = [-1; -0.75; -0.5; -0.25; 0; 0.25; 0.5; 0.75; 1];
    y = sin(pi*t);
    points = horzcat(t, y);
    polynomial = newton(points)
    
    plot_range = linspace(-1, 1, 100);
    plot_func = polyval(polynomial, plot_range);
    plot_func2 = feval(@(t) sin(pi*t), plot_range);
    figure(1); plot(plot_range, plot_func); axis('equal');
    
    max_error = 0;
    for i = 1:100
        mid = plot_range(i);
        val_i = polyval(polynomial, mid);
        real_i = sin(pi*mid);
        
        error_i = abs((val_i - real_i) / real_i);
        max_error = max(max_error, error_i);
    end
    max_error
end