%% Function for numerical integration using Newton's method
%% @input f : function f
%% @input points : points to be interpolated 
%%
%% @output polynomial : Polynomial for Newton, in [p1, p2, p3, ..., pm], where newton = p1*t^(m-1) + p2*t^(m-2) + ... + pm

function [polynomial] = newton(points)
    m = length(points);
    a = calculateNewtonCoef(points);

    polynomial = zeros(m, 1);
    helper = zeros(m, 1);
    helper(m) = 1;
    
    for i = 1:m
      polynomial = polynomial + helper * a(i);
      if i ~= m
          helper = [helper(2:m); 0] - points(i, 1) * helper;
      end
    end 
end
