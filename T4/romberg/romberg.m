%% Function for numerical integration using Romberg's method
%% @input f    : function f
%% @input str_x : string of variable to be integrate in respect to
%% @input interval : Integration interval
%% @input partitions    : Number of rows in Romberg table
%%
%% @output x_n   : Result of Romberg Method
%% @output counter : How many functions that evaluated (efficiency)

function [x_n, counter] = romberg(f, str_x, interval, partitions)

    if nargin ~= 4
        error('Expecting 4 arguments, an equation, integration interval a, b ',
        	'number of rows n');
    end

    a = interval(1);
    b = interval(2);
    n = log2(partitions);

	h = (b - a) ./ (2.^(0:n-1));
    r(1,1) = (b - a) * (f(a) + f(b)) / 2;
    counter = 2;
    for j = 2:n
        subtotal = 0;
        for i = 1:2^(j-2) 
            subtotal = subtotal + f( a + (2 * i - 1) * h(j));
            counter += 1;
        end
        r(j,1) = r(j-1,1) / 2 + h(j) * subtotal;
        for k = 2:j
            r(j,k) = (4^(k-1) * r(j,k-1) - r(j-1,k-1)) / (4^(k-1) - 1);
        end
    end;
    x_n = r(n,n);
end