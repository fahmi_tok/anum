%% Function for solving integration of e^y  cos(x) dx dy
%% @input f    : inline function containing the equation
%% @input sym_str : list of symbols of x in string
%% @input intervals : list of vectors interval for symbols
%% @input n    : Number of rows in Romberg table
%%
%%
%% @output x_n : final result of x
%% @output err : final error

function [x_n, counter, err] = solve2a(n)
    [x_res1, counter1] = romberg(@(x) cos(x), ['x'], [[0, pi/2]], n);

    [x_res, counter] = romberg(@(y) x_res1 * exp(y), ['y'], [[0, 1]], n);

    x_n = x_res;
    counter += counter1;
    err = abs(1.7182818284590450907955982984276 - x_n);
end