function [x] = leastSquareGivensRotation (A, b)
  [Q, R, P] = qrGivens(A);
  [m, n] = size(A);
  x = zeros(n,1);

  %permutate b
  % tmp = b;
  % for i = 1:m
  %   b(i) = tmp(p(i));
  % endfor
  Qb = Q' * b;
  [z] = backwardSubstitution(R, Qb);

  for i = 1:n
    x(i) = z(P(i));
  endfor

endfunction