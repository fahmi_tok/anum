% qrgivens.m
function [Q, R, P] = qrGivens(A)
  [m,n] = size(A);
  Q = eye(m);
  R = A;
  P = 1:n;
  lens = 1:n;
  for i = 1:n
    lens(i) = A(:,i)'*A(:,i);
  endfor


  for j = 1:n
    %pivoting
    %find collumn with max norm
    [maxElem, maxElemCollumn] = max(lens(j:n));
    maxElemCollumn = j + maxElemCollumn - 1;  
    %swap A
    tmp = R(:,j); R(:,j) = R(:,maxElemCollumn); R(:,maxElemCollumn) = tmp;
    %swap p
    tmp = P(j); P(j) = P(maxElemCollumn); P(maxElemCollumn) = tmp;
    %swap l
    tmp = lens(j); lens(j) = lens(maxElemCollumn); lens(maxElemCollumn) = tmp;

    for i = m:-1:(j+1)
      % Compute Givens Rotation Matrix
      G = eye(m);
      h = hypot(R(i-1,j),R(i,j));
      if h == 0
        continue
      endif
      c = R(i-1,j) / h;
      s = R(i,j) / h;
      G([i-1, i],[i-1, i]) = [c -s; s c];
      R = G'*R;
      Q = Q*G;
    end
    
    % Update lens
    for i = j+1:n
      lens(i) = lens(i) - R(j,i)^2;
    end
  end
end