function [M, N, b] = splitMatrix(A, b, omega)
    b = omega * b;
    M =  omega * tril(A, -1) + diag(diag(A));
    N = -omega * triu(A, 1) + (1.0 - omega) * diag(diag(A));
end