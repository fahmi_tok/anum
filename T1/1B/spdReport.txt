Using omega = 0.874785
Iteration limit = 10000
Tolerable error = 1e-09
SORtime: 1.470e-03 sec.
SORiter: 12.
Using omega = 0.840322
Iteration limit = 10000
Tolerable error = 1e-09
SORtime: 1.143e-03 sec.
SORiter: 13.
Using omega = 0.828906
Iteration limit = 10000
Tolerable error = 1e-09
SORtime: 1.503e-03 sec.
SORiter: 13.
Using omega = 0.828547
Iteration limit = 10000
Tolerable error = 1e-09
SORtime: 4.707e-03 sec.
SORiter: 12.
Using omega = 0.828432
Iteration limit = 10000
Tolerable error = 1e-09
SORtime: 7.832e-02 sec.
SORiter: 12.
Using omega = 0.828428
Iteration limit = 10000
Tolerable error = 1e-09
SORtime: 6.039e-01 sec.
SORiter: 12.
Using omega = 0.828428
Iteration limit = 10000
Tolerable error = 1e-09
SORtime: 2.047e+00 sec.
SORiter: 12.
Using omega = 0.828427
Iteration limit = 10000
Tolerable error = 1e-09
SORtime: 4.593e+00 sec.
SORiter: 12.
Using omega = 0.828427
Iteration limit = 10000
Tolerable error = 1e-09
SORtime: 8.425e+00 sec.
SORiter: 12.
Using omega = 0.828427
Iteration limit = 10000
Tolerable error = 1e-09
SORtime: 1.401e+01 sec.
SORiter: 12.