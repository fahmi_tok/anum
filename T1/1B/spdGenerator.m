function [M, b] = spdGenerator(n)
	M = rand(n,n);
	M = 0.5*(M+M');
	M = M + n*eye(n);
	b = n*rand(n,1);