%% Main function for calculating succesive Over Relaxation method
%% @input A    : Square matrix nxn
%% @input b    : vector nx1
%% @output x   : vector nx1 that store the solution with smallest error
%%               for linear system Ax=b
%% @output err : error from the last iteration
function [x, err] = successiveOverRelaxation(A,b)
    %% Expecting 2 arguments, a square matrix and a vector
    if nargin ~= 2
        print_usage();
    end

    [n,m] = size(A);
    if n ~= m
        error('Only square systems');
    end

    [k] = size(b);
    if k ~= m
        error('Size of b is not equal to number of column of A');
    end

    %% CONSTANT BEGIN %%
    MAX_ITERATION = 1000;
    EPS = 1e-3;
    INITIAL_ERR = 100;
    OMEGA = 1.3;
    %% CONSTANT END %%

    iter = 0;
    x = ones(n,1);
    err = INITIAL_ERR;

    fprintf('Using omega = %d\n', OMEGA);
    fprintf('Iteration limit = %d\n', MAX_ITERATION);
    fprintf('Tolerable error = %d\n', EPS);

    [ M, N, b ] = splitMatrix(A, b, OMEGA);

    while err > EPS && iter < MAX_ITERATION
        iter = iter + 1;
        prev_x = x;

        x = M \ (N * prev_x + b);

        err = norm((M-N)*x - b) / norm(b);
    end

    if norm((M-N)*x - b) / norm(b) > EPS
        warning('this computation is not accurate because error still larger than 1e-3')
        break
    end
