function [Q, R, p] = householder (A)
  %p is permutation vector instead of permutation matrix for better complexity
  %lens is vector of collumn norms for column pivoting to minimize errors
  
  [m, n] = size(A);
  Q = eye(m);
  p = 1:n;
  lens = 1:n;
  for i = 1:n
    lens(i) = A(:,i)'*A(:,i);
  endfor
  
  %loop columns
  for i = 1:n
    %pivoting
    %find max element in row i
    [maxElem, maxElemCollumn] = max(lens(i:n));
    maxElemCollumn = i + maxElemCollumn - 1;  
    %swap A
    tmp = A(:,i); A(:,i) = A(:,maxElemCollumn); A(:,maxElemCollumn) = tmp;
    %swap p
    tmp = p(i); p(i) = p(maxElemCollumn); p(maxElemCollumn) = tmp;
    %swap lens
    tmp = lens(i); lens(i) = lens(maxElemCollumn); lens(maxElemCollumn) = tmp;
    
    %init v
    v = A(i:m, i);
    v(1) = v(1) + sign(v(1))*norm(v);
    
    %multiply with H_i
    tmp = 2 / (v' * v); %avoid compute this constant twice
    A(i:m, i:n) = A(i:m, i:n) - (tmp * v) * (v' * A(i:m, i:n));
    Q(:, i:m) = Q(:, i:m) - (Q(:, i:m) * v) * (tmp * v');
    
    %update lens
    for j = i+1:n
      lens(j) = lens(j) - A(i,j)^2;
    endfor
    
  endfor
  R = A;
endfunction
