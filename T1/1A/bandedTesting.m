function bandedTesting(type)
    [A, b] = bandedGenerator(5, type);
    tic, [x, err] = solveLUFact(A, b); timeSolveLUFact = toc;
    fprintf('LUFacttime: %1.3e sec.\n',timeSolveLUFact);
    fprintf('LUFacterror: %1.3e\n', err);

    [A, b] = bandedGenerator(10, type);
    tic, [x, err] = solveLUFact(A, b); timeSolveLUFact = toc;
    fprintf('LUFacttime: %1.3e sec.\n',timeSolveLUFact);
    fprintf('LUFacterror: %1.3e\n', err);

    [A, b] = bandedGenerator(50, type);
    tic, [x, err] = solveLUFact(A, b); timeSolveLUFact = toc;
    fprintf('LUFacttime: %1.3e sec.\n',timeSolveLUFact);
    fprintf('LUFacterror: %1.3e\n', err);

    [A, b] = bandedGenerator(100, type);
    tic, [x, err] = solveLUFact(A, b); timeSolveLUFact = toc;
    fprintf('LUFacttime: %1.3e sec.\n',timeSolveLUFact);
    fprintf('LUFacterror: %1.3e\n', err);

    [A, b] = bandedGenerator(500, type);
    tic, [x, err] = solveLUFact(A, b); timeSolveLUFact = toc;
    fprintf('LUFacttime: %1.3e sec.\n',timeSolveLUFact);
    fprintf('LUFacterror: %1.3e\n', err);

    [A, b] = bandedGenerator(1000, type);
    tic, [x, err] = solveLUFact(A, b); timeSolveLUFact = toc;
    fprintf('LUFacttime: %1.3e sec.\n',timeSolveLUFact);
    fprintf('LUFacterror: %1.3e\n', err);

    [A, b] = bandedGenerator(1500, type);
    tic, [x, err] = solveLUFact(A, b); timeSolveLUFact = toc;
    fprintf('LUFacttime: %1.3e sec.\n',timeSolveLUFact);
    fprintf('LUFacterror: %1.3e\n', err);

    [A, b] = bandedGenerator(2000, type);
    tic, [x, err] = solveLUFact(A, b); timeSolveLUFact = toc;
    fprintf('LUFacttime: %1.3e sec.\n',timeSolveLUFact);
    fprintf('LUFacterror: %1.3e\n', err);

    [A, b] = bandedGenerator(2500, type);
    tic, [x] = solveLUFact(A, b); timeSolveLUFact = toc;
    fprintf('LUFacttime: %1.3e sec.\n',timeSolveLUFact);

    [A, b] = bandedGenerator(3000, type);
    tic, [x, err] = solveLUFact(A, b); timeSolveLUFact = toc;
    fprintf('LUFacttime: %1.3e sec.\n',timeSolveLUFact);
    fprintf('LUFacterror: %1.3e\n', err);
end