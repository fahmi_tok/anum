function [M, b] = bandedGenerator(n, type)
    M = eye(n);
    p = randi([0, n-1]);
    q = randi([0, n-1]);
    % keep randoming until p & q aren't equal to n-1
    while(p == n-1 && q == n-1)
        p = randi([0, n-1]);
        q = randi([0, n-1]);
    end

    % fprintf(int2str(q))
    % fprintf('\n')
    % fprintf(int2str(p))
    % fprintf('\n')

    for i=1:n
        M(i,i) = randi([1, 100]);
    end

    for i=1:q
        for j=1:n
            if (j+i <= n)
                if (strcmp('dense', type))
                    chance = randi([1,10]);
                    if (chance > 4)
                        M(j+i,j) = randi([1, 100]);
                    else
                        M(j+i,j) = 0;
                    end
                else
                    chance = randi([1,10]);
                    if (chance > 4)
                        M(j+i,j) = 0;
                    else
                        M(j+i,j) = randi([1, 100]);
                    end
                end
            end
        end
    end

    for i=1:p
        for j=1:n
            if (j+i <= n)
                if (strcmp('dense', type))
                    chance = randi([1,10]);
                    if (chance > 4)
                        M(j,j+i) = randi([1, 100]);
                    else
                        M(j,j+i) = 0;
                    end
                else
                    chance = randi([1,10]);
                    if (chance > 4)
                        M(j,j+i) = 0;
                    else
                        M(j,j+i) = randi([1, 100]);
                    end
                end
            end
        end
    end

    b = randi([1, 100], n, 1);
end