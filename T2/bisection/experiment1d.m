function total_time = experiment1d()
    f = 'x^4-6*x^3+12*x^2 -10*x+3';
	total_time = 0;

	tic, [x, err] = bisection(f, 0.9, 3.2, 1e-4); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -36, 36, 1e-4); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -18, 26, 1e-4); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -4, 8, 1e-4); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -8, 14, 1e-4); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

   	total_time = total_time/5;
end