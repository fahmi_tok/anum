function total_time = experiment1e()
    f = 'x^2-cos(pi*x)';
	total_time = 0;

	tic, [x, err] = bisection(f, -1.4, 1.4, 1e-12); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -8.5, 8.5, 1e-12); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -3.6, 3.6, 1e-12); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -12, 12, 1e-12); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -10, 10, 1e-12); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

   	total_time = total_time/5;
end