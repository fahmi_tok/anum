function total_time = experiment1e()
    f = '10*e^-x*sin(2*pi*x)-2';
	total_time = 0;

	tic, [x, err] = bisection(f, -2, 2, 1e-12); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -12, 12, 1e-12); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -5, 5, 1e-12); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -10, 10, 1e-12); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -15, 15, 1e-12); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

   	total_time = total_time/5;
end