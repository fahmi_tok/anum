function [x, err] = bisection(f, a, b, tol)
    %% Expecting 4 arguments, an equation in string
    %% a guess for initial a and b, and the limit for error toleration
    if nargin ~= 4
        error('Expecting 4 arguments, an equation in string, a guess for initial a and b, and the limit for error toleration');
    end
    % Load symbolic package and turn off annoying warning
    pkg load symbolic;
    warning('off','all');

    %constraint for max iteration
    maxIter = 100;
    floatingPoint = -log10(tol);
    syms x;
    fx = sym(f);


    iter = 0;

    x = (a + b)/2;
    % err = abs(vpa(subs(fx, x), floatingPoint));

    while(b - a > tol)
        if(vpa(subs(fx, a), floatingPoint)*vpa(subs(fx, x), floatingPoint) < 0)
            b = x;
        else
            a = x;  
        end
        
        % fprintf('At iteration %d, x = %e and error = %e\n', iter, double(x), double(err));
    end
    err = vpa(subs(fx, x), floatingPoint);
end