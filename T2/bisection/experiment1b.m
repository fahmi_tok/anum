function total_time = experiment1b()
	f = 'x^x-2';
	total_time = 0;

	tic, [x, err] = bisection(f, -1, 2, 1e-4); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -8.5, 9.5, 1e-4); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -7, 6, 1e-4); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -10, 5, 1e-4); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -3.5, 6.5, 1e-4); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

   	total_time = total_time/5;
end