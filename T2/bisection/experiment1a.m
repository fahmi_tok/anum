function total_time = experiment1a()
	f = 'x^3 + 2*x^2 + 10*x - 20';
	total_time = 0;

	tic, [x, err] = bisection(f, -2.2, 1.5, 1e-12); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -12, 12, 1e-12); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -5, 3, 1e-12); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -10, 5, 1e-12); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

    tic, [x, err] = bisection(f, -4, 4, 1e-12); 
	timeSolveBisec = toc;
    fprintf('BisectionTime: %1.3e sec.\n',timeSolveBisec);
    x
    err
    total_time += time;

   	total_time = total_time/5;
end