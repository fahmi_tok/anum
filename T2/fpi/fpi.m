
% %% g(x) =  20 / (x^2 + (2*x) + 10)
% function y = g3 (x)
%     y = 20 / (x^2 + (2*x) + 10);
% end

%% solve x^3 + 2*x^2 + 10*x - 20 = 0
function [x, errDiff] = fpi(f, g, tol, x0)
    %% Expecting 4 arguments, an equation in string, a g(x)
    %% a guess for initial x,  and the limit for error toleration
    if nargin ~= 4
        error('Expecting 4 arguments, an equation in string, an g(x), a guess for initial x, and the limit for error toleration');
    end

    % Load symbolic package and turn off annoying warning
    pkg load symbolic;
    warning('off','all');

    %% constant for max iteration
    maxIter = 100;
    floatingPoint = -log10(tol);
    syms x;
    fx = sym(f);
    gx = sym(g);

    % set initial values and error
    x = x0;
    err = 100;
    iter = 0;

    while (iter < maxIter && (err > tol))
        xnew = vpa(subs(gx, x), floatingPoint)
        err = abs(xnew - x)
        x = xnew;
        iter = iter + 1
        fprintf('At iteration %d, x = %e and error = %e\n', iter, double(x), double(err))
    end
    errDiff = vpa(subs(fx, x), floatingPoint);
end