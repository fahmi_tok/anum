%% Function for solving nonlinear equation using modified newton's method
%% @input fx    : String containing equation of the nonlinear equation
%%				ex = 'x^2+sin(x)'
%% @input x0 	: initial guess of x
%% @input tol   : error toleration,
%% @output err : error from the last iteration
function[x_res, err, iter] = modifiedNewton(fx, x0, tol)

	%% Expecting 3 arguments, an equation in string,
	%% a guess for initial x,  and the limit for error toleration
    if nargin ~= 3
        error('Expecting 3 arguments, an equation in string, a guess for initial x, and the limit for error toleration');
    end


	% Load symbolic package and turn off annoying warning
	pkg load symbolic;
	warning('off', 'all');

	%% constant for max iteration
	maxIter = 100;

	syms x;
	fx = sym(fx);
	df = diff(fx, x);

	%% set x_res, multiplicity,  and set initial error
	x_res = x0;
  m = guessMultiplicity(fx, df, x0);
	err = 100;
	iter = 0;

	while iter < maxIter && tol < err

		x_prev = x_res;
		x_res = x_prev - m * vpa(subs(fx, x, x_prev))/vpa(subs(df, x, x_prev));

		err = abs(x_res - x_prev);
		iter = iter + 1;
		%fprintf('At iteration %d, x = %e and error = %e\n', iter, double(x_res), double(err));
	end

end