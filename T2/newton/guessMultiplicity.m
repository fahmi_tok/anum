function [p] = guessMultiplicity (fx, df, x_0)
  syms x;
  x_1 = x_0 - vpa(subs(fx, x, x_0))/vpa(subs(df, x, x_0));
  x_2 = x_1 - vpa(subs(fx, x, x_1))/vpa(subs(df, x, x_1));
  
  p = 0.5 + (x_0 - x_1) / (x_2 - 2*x_1 + x_0);
endfunction
