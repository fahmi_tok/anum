function [] = newtonTesting()

	fxs = {'x^3+2*x^2+10*x-20'; 'x^x-2'; '10*e^(-x)*sin(2*pi*x)-2'; 'x^4-6*x^3+12*x^2-10*x+3'; 'x^2-cos(pi*x)'};
	tols = [1e-4; 1e-6; 1e-8; 1e-10; 1e-12];


	for j = 1:5 %% sering error grgr turunannya
		fx = fxs{j};
		disp(fx);
		x0 = guess(5);
		disp(x0);

		for k = 1:5

			avg_time = 0;
			avg_err = 0;
			tol = tols(k);
			disp(tol);

			for i = 1:5
				tic, [x_res, err] = newton(fx, x0(i), tol); time = toc;
				avg_time += time;
				avg_err += double(err);
			end

			fprintf('time : %e\n' ,avg_time/5);
			fprintf('err : %e\n' ,avg_err/5);

		end
	end
end