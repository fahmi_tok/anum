function[result] = guess(k)
	% Load symbolic package and turn off annoying warning

	for i = 1:k
		result(i) = 5*rand;
	end

end
