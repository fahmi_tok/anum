%% Function for nonlinear optimization using Limited Memory BFGS method
%% @input f    : String containing equation of the nonlinear equation
%%				ex = 'x^2+sin(x)'
%% @input sym_x0_str : list of symbols of x in string
%% @input x0 	: initial guess of x
%% @input tol   : error toleration,
%% @output x_n : final result of x respect to the tolerable error,
%% @output err : final error
function[x_n, err] = lbfgsGriewank(x, tol)

	%% Expecting 2 arguments
	%% example lbfgsGriewank(8, 1e-5)
    if nargin ~= 2
        error('Expecting 2 arguments');
    end

	% Load symbolic package and turn off annoying warning
	pkg load symbolic;
	warning('off', 'all');

	%% constant for max iteration
	maxIter = 100;

    %% get initial values
    x_0 = [];
    base_0 = [-400,-200,200,400];
    for i=1:x
        x_0 = [x_0; base_0(mod(i-1,4)+1)];
    end
	%% input functions for poblano
	function [f_val,g_val] = FUN(x_0)
		f_val = griewank(x_0);
		g_val = gradGriewank(x_0);
	end
	 
	out = lbfgs(@(x) FUN(x), x_0, 'Display', 'final', 'StopTol', tol, 'RelFuncTol', tol, 'MaxIters', maxIter, 'TraceFunc', true);
	x_n = out.X;
    x_prev_n = out.Iters - 1;
	if (x_prev_n == 0)
		err = abs(out.F);
	else
		err = abs(out.F - out.TraceFunc(x_prev_n));
	end
end

function [result] = griewank(x)
    n = length(x);
    s = 0;
    p = 1;
    for j = 1:n; s = s + x(j)*x(j); end
    for j = 1:n; p = p * cos(x(j)/sqrt(j)); end
    result = 1 + (s / 4000) - p;
end

function [result] = gradGriewank(x)
    n = length(x);
    result = zeros(n, 1);
    for i = 1:n
        c = 1;
        for j = 1:n
            if j == 1
                c = c * sin(x(j)/sqrt(j)) / sqrt(j);
            else
                c = c * cos(x(j)/sqrt(j));
            end
        end
        result(i, 1) = x(i) / 2000 + c;
    end
end