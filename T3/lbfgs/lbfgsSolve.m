%% Function for nonlinear optimization using Limited Memory BFGS method
%% @input f    : String containing equation of the nonlinear equation
%%				ex = 'x^2+sin(x)'
%% @input sym_x0_str : list of symbols of x in string
%% @input x0 	: initial guess of x
%% @input tol   : error toleration,
%% @output x_n : final result of x respect to the tolerable error,
%% @output err : final error

function[x_n, err] = lbfgsSolve(f, sym_x_str, x0, tol)

	%% Expecting 4 arguments, an equation in string,
	%% example lbfgsSolve('0.5*x*x+2.5*y*y', ['x'; 'y'], [1; 2], 1e-5)
    if nargin ~= 4
        error('Expecting 4 arguments, an equation in string, list of sysmbols of x in string, ',
        	'a guess for initial x, and the limit for error toleration');
    end

	% Load symbolic package and turn off annoying warning
	pkg load symbolic;
	warning('off', 'all');

	%% constant for max iteration
	maxIter = 100;

	%% f and grad_f needed for poblano
	f = sym(f);
	sym_x = {};
	sym_x_str = cellstr(sym_x_str);
	for idx = 1:length(sym_x_str)
		tmp = sym(sym_x_str{idx});
		sym_x{idx} = tmp;
	end
	grad_f = gradient(f, sym_x);

	%% input functions for poblano
	function [f_val,g_val] = FUN(x0)
		f_val = double(subs(f, sym_x', x0));
		g_val = double(subs(grad_f, sym_x', x0));
	end
	 
	out = lbfgs(@(x) FUN(x), x0, 'Display', 'final', 'StopTol', tol, 'RelFuncTol', tol, 'MaxIters', maxIter, 'TraceFunc', true);
	x_n = out.X;
	x_prev_n = out.Iters - 1;
	err = abs(out.F - out.TraceFunc(x_prev_n));
end