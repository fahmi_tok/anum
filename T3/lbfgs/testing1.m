function [] = testing1()

	tols = [1e-4; 1e-6; 1e-8; 1e-10; 1e-12];
    fx = '0.5*x*x+2.5*y*y';
    sym_x_str = ['x', 'y'];
    x0 = [1;2];
    for k = 1:5
        tol = tols(k);
        disp(tol);
        tic, [x_res, err] = lbfgsSolve(fx, sym_x_str,x0,tol); time = toc;
        fprintf('time : %e\n' ,time);
        fprintf('err : %e\n' ,err);
    end
end