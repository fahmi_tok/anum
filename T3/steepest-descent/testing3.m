function [] = testing3()

	tols = [1e-4; 1e-6; 1e-8; 1e-10; 1e-12];
    [fx, sym_x_str, x0] = griewank(8);
    for k = 1:1
        avg_time = 0;
        avg_err = 0;
        tol = tols(k);
        disp(tol);

        tic, [x_res, err] = steepest(fx, sym_x_str,x0,tol); time = toc;
        avg_time += time;
        avg_err += double(err);

        fprintf('time : %e\n' ,avg_time);
        fprintf('err : %e\n' ,avg_err);
    end
end