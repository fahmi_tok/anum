function [] = testing1()

	tols = [1e-4; 1e-6; 1e-8; 1e-10; 1e-12];
    fx = '0.5*x^2+2.5*y^2';
    sym_x_str = ['x'; 'y'];
    x0 = [1;2];
    for k = 1:5
        avg_time = 0;
        avg_err = 0;
        tol = tols(k);
        disp(tol);

        for i = 1:1
            tic, [x_res, err] = steepest(fx, sym_x_str,x0,tol); time = toc;
            avg_time += time;
            avg_err += double(err);
        end

        fprintf('time : %e\n' ,avg_time);
        fprintf('err : %e\n' ,avg_err);
    end
end