%% Function for finding alpha using exact line search
%% to minimize f(alpha) with newton approximation
%% @input fx : String of the function, here alpha must be written as x
%% @input tol : error tolerance
%% @output x_res : the resulted alpha
function [x_res] = lineSearch(fx, tol)
  pkg load symbolic;
  warning('off', 'all');
  
  syms x;
  fx = sym(fx);
  df = diff(fx, x);
  [x_res, err] = newton(df, 1, tol);
end