%% Function for nonlinear optimization using steepest descent method
%% @input f : String of the function ex = 'x^2 + sin(y)'
%% @input var_sym : symbols used for variables in the function
%% @input x_0 : initial guess for the variables
%% @input tol : error tolerance
%% @output x_res : the resulted optimization with respect to tolerance
%% @output err : final error
function[x_res, err] = steepest(f, var_sym, x_0, tol)
  pkg load symbolic;
  f = sym(f);
  [n, m] = size(var_sym);
  
  grad_f = [];
  for i=1:n
    tmp = gradient(f, var_sym(i,:));
    grad_f = vertcat(grad_f, tmp);
  endfor
  hessian_f = [];
  for i=1:n
    tmp = [];
    for j=1:n
      tmp2 = gradient(grad_f(i), var_sym(j,:));
      tmp = horzcat(tmp, tmp2);
    endfor
    hessian_f = vertcat(hessian_f, tmp);
  endfor
  
  maxIter = 100;
  iter = 0;
  
  err = 100;
  while iter < maxIter && tol < err
    grad_x0 = grad_f;
    hessian_x0 = hessian_f;
    for i=1:n
      grad_x0 = subs(grad_x0, var_sym(i,:), x_0(i));
      hessian_x0 = subs(hessian_x0, var_sym(i,:), x_0(i));
    endfor
    
    [~,p] = chol(double(hessian_x0)); %p = 0 if positive definite
    
    %is local minimum
    if grad_x0 ==0 && p == 0
      break;
    endif
    
    %construct p_0
    p_0 = double(-grad_x0);
    
    %here x is alpha
    %it's okay to use x since p_0 is already variable-free
    syms x;
    p_0 = p_0 * x;
    
    %g is f(x_0+alpha*p_0)
    g = f;
    for i=1:n
      g = subs(g, var_sym(i,:), x_0(i) + p_0(i));
    endfor
    
    x_1 = x_0 + subs(p_0, x, lineSearch(g, tol));
    
    f_0 = f;
    f_1 = f;
    for i=1:n
      f_0 = subs(f_0, var_sym(i,:), x_0(i));
      f_1 = subs(f_1, var_sym(i,:), x_1(i));
    endfor
    err = abs(f_1 - f_0);
    x_0 = x_1;
    iter = iter + 1;
  end
  x_res = x_0;
end