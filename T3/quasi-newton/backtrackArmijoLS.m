%% Helper function for finding a_k using backtracking armijo LS method
%% @input f    : input function
%% @input sym_x : value symbol mapping for each entry in x
%% @input x : current x
%% @input p : current p
%% @input g : current g
%% @input tol : error tolerance
%% @output alpha : optimal a (step)

function[a] = backtrackArmijoLS(f, sym_x, x, p, g, tol)

	%% Expecting 6 arguments and called via quasiNewton method
    if nargin ~= 6
        error('Expecting 6 arguments');
    end

    a_k = 1;
    b = 1e-4;
    t = 0.5;

    maxIter = 100;
    iter = 0;

	while iter < maxIter && double(subs(f, sym_x, x + a_k*transpose(p)))  - (double(subs(f, sym_x, x)) + a_k*b*transpose(g)*p) > tol
		a_k = t*a_k;
        iter += 1;
	end

	a = a_k;

end