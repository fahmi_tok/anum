%% Function for nonlinear optimization using quasi newton's method
%% @input f    : String containing equation of the nonlinear equation
%%				ex = 'x^2+sin(x)'
%% @input sym_x_str : list of symbols of x in string
%% @input x_0 	: initial guess of x
%% @input tol   : error toleration,
%% @output x_n : final result of x respect to the tolerable error,
%% @output err : final error

function[x_n, err] = quasiNewton(f, sym_x_str, x_0, tol)

	%% Expecting 4 arguments
	%% example quasiNewton('0.5*x*x+2.5*y*y', ['x'; 'y'], [1, 2], 1e-5)
    if nargin ~= 4
        error('Expecting 4 arguments, an equation in string, list of sysmbols of x in string, ',
        	'a guess for initial x, and the limit for error toleration');
    end


	% Load symbolic package and turn off annoying warning
	pkg load symbolic;
	warning('off', 'all');

	%% constant for max iteration
	maxIter = 100;

	%% constructing grad f
	f = sym(f);
	sym_x = {};
	sym_x_str = cellstr(sym_x_str);
	for idx = 1:length(sym_x_str)
		tmp = sym(sym_x_str{idx});
		sym_x{idx} = tmp;
	end
	grad_f = gradient(f, sym_x);

	%% initial B (B0)
	[_, n] = size(sym_x);
	B_k = eye(n);
	x_k = x_0;
	err = 1e5;

	iter = 0;
	while iter < maxIter && err > tol

		g_k = double(subs(grad_f, sym_x, x_k));
		p_k = -B_k * g_k;
		a_k = backtrackArmijoLS(f, sym_x, x_k, p_k, g_k, tol);

		x_k_nxt = x_k + a_k*transpose(p_k);

		g_k_nxt = double(subs(grad_f, sym_x, x_k_nxt));
		dlt_k = x_k_nxt - x_k;
		y_k = g_k_nxt - g_k;

		err = double(abs(subs(f, sym_x, x_k_nxt) - subs(f, sym_x, x_k)));
		B_k = SR1(B_k, dlt_k, y_k);
		x_k = double(x_k_nxt);

		iter++;
	end

	x_n = x_k;

end