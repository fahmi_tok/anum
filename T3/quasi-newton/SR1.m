%% Helper function for updating B_k with SR1
%% @input B    : previous B
%% @input dlt : current delta
%% @input y : current y
%% @output B_nxt : next B

function[B_nxt] = SR1(B, dlt, y)

	%% Expecting 3 arguments and called via quasiNewton method
    if nargin ~= 3
        error('Expecting 3 arguments');
    end

    dlt = transpose(dlt);
    B_nxt = B + ( ((dlt - B*y)*transpose(dlt - B*y)) / ((transpose(dlt - B*y)) * dlt) );

end