function [] = testing2(n)

	tols = [1e-4; 1e-6; 1e-8; 1e-10; 1e-12];
    [fx, sym_x_str, x0] = griewank(n);
    for k = 1:5
        tol = tols(k);
        disp(tol);
        tic, [x_res, err] = newtonSolve(fx, sym_x_str,x0',tol); time = toc;
        fprintf('time : %e\n' ,time);
        fprintf('err : %e\n' ,err);
    end
end