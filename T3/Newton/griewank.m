function[f, var_sym, x_0] = griewank(n)
  f = '1';
  g = '';
  base_0 = [-400,-200,200,400];
  cur = 'a';
  x_0 = [];
  var_sym = [];
  pos = 1;
  
  for i=1:n
    x_0 = [x_0; base_0(mod(i-1,4)+1)];
    f = [f,'+', cur, '^2/4000'];
    var_sym = [var_sym; cur];
    if length(g) > 0
      g = [g, '*'];
    endif
    g = [g,'cos(',cur,'/sqrt(',int2str(i),'))'];
    
    if cur(pos) == 'z'
      pos = pos - 1;
    else
      cur(pos) = cur(pos) + 1;
    endif
    
    if (pos == 0)
      tmp = '';
      for i = 1:length(cur)
        tmp = [tmp, 'a'];
      endfor
      cur = [tmp, 'a'];
      pos = length(cur);
    endif
 endfor
 f = [f, '-', g];
end