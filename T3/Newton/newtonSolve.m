%% Function for nonlinear optimization using Newton Method
%% @input f : String containing equation of the nonlinear equation
%%	ex = 'x^2+sin(x)'
%% @input sym_x0_str : list of symbols of x in string
%% @input x0 : initial guess of x
%% @input tol : error toleration,
%% @output x_n : final result of x respect to the tolerable error,
%% @output err : final error

function[x_n, err] = newtonSolve(f, sym_x_str, x0, tol)

 %% Expecting 4 arguments, an equation in string,
 %% example newtonSolve('0.5*x*x+2.5*y*y', ['x'; 'y'], [1, 2], 1e-5)
    if nargin ~= 4
        error('Expecting 4 arguments, an equation in string, list of sysmbols of x in string, ',
         'a guess for initial x, and the limit for error toleration');
    end

 % Load symbolic package and turn off annoying warning
 pkg load symbolic;
 warning('off', 'all');

 %% constant for max iteration
 maxIter = 100;
	%% f and grad_f and Hessian
	f = sym(f);
	sym_x = {};
	sym_x_str = cellstr(sym_x_str);

    for idx = 1:length(sym_x_str)
        tmp = sym(sym_x_str{idx});
        sym_x{idx} = tmp;
    end
    gradient(f, sym_x);
    hessian_f = hessian(f);

    iter = 0;
    x_k = x0;
    while(iter < maxIter)
        H = double(subs(hessian_f, sym_x, x_k));
        grad = -(double(subs(grad_f, sym_x, x_k)));

        v = linsolve(H, grad)
        iter++
        err = double(subs(f, sym_x, x_k));
        x_k = x_k + v'
        err = abs(double(subs(f, sym_x, x_k)) - err);
        if ( abs(double(subs(grad_f, sym_x, x_k))) <= tol)
            break
        end
    end
    x_n = x_k;
end