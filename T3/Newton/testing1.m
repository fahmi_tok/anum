function [] = testing1()

	tols = [1e-4; 1e-6; 1e-8; 1e-10; 1e-12];
    fx = '2*pi*r*(r+t) + a*(pi*r*r*t - 400)';
    sym_x_str = ['r', 't','a'];
    x0 = [1,1,-0.5];
    for k = 1:5
        tol = tols(k);
        disp(tol);
        tic, [x_res, err] = newtonSolve(fx, sym_x_str,x0,tol); time = toc;
        fprintf('time : %e\n' ,time);
        fprintf('err : %e\n' ,err);
    end
end